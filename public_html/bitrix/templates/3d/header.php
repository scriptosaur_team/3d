<?
if(!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true)
	die();
?><!DOCTYPE html>
<html>
	<head>
		<title><?$APPLICATION->ShowTitle();?></title>
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<meta http-equiv="x-ua-compatible" content="ie=edge">
		<link rel="shortcut icon" type="image/x-icon" href="/favicon.ico" />
        <link href="https://fonts.googleapis.com/css?family=Open+Sans|Roboto:300,700,900&subset=cyrillic" rel="stylesheet">
        <link rel="stylesheet" href="<?=SITE_TEMPLATE_PATH?>/styles/styles.css">
<?if($USER->IsAdmin()):?>
        <?$APPLICATION->ShowHead()?>
<?endif?>
	</head>
	<body>
		<?$APPLICATION->ShowPanel();?>
