<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<ul class="workExamples">
<?foreach($arResult["ITEMS"] as $k => $arItem):?>
<?if($k % 2 == 0 && $k > 0):?>
    <li class="workExamples__itemSeparator"></li>
<?endif?>
	<?
	$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
	$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
	?>
	<li class="workExamples__item" id="<?=$this->GetEditAreaId($arItem['ID']);?>">
		<a href="<?=$arItem["DETAIL_PAGE_URL"]?>" class="workExamples__itemLink">
            <div class="workExamples__item_picture">
                <div class="workExamples__item_pictureHolder">
                    <img src="<?=$arItem["PREVIEW_PICTURE"]["SRC"]?>">
                </div>
            </div>
            <span class="workExamples__itemName"><?=$arItem["NAME"]?></span>
        </a>
	</li>
<?endforeach;?>
</ul>
