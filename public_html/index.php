<?
require($_SERVER['DOCUMENT_ROOT'].'/bitrix/header.php');
$APPLICATION->SetTitle('Главная');
?>


<section class="section section1">
    <div class="container">
        <img class="section1__logo fade-in" src="<?=SITE_TEMPLATE_PATH?>/images/logo.png">
        <div class="section__headGroup fade-in">
            <h2 class="section__header section1__header">Фотографируй в 3D!</h2>
            <p class="section__subHeader section1__subHeader">Создавайте фотографии, которые можно вращать</p>
        </div>
        <p class="section1__description fade-in">Поворотный стол предназначен для создания 3D-фотографий - анимированные изображения на основе серии обычных фотографий, выполненных при разной ориентации объекта. На экране компьютера такое изображение можно вращать, приближать и разглядывать детали!</p>
    </div>
    <div class="section1__table">
        <div class="section1__tableItem">
            <img src="<?=SITE_TEMPLATE_PATH?>/images/table_with_item.png" class="section1__tableItemImage">
            <img src="<?=SITE_TEMPLATE_PATH?>/images/table_with_item_shadow.png" class="section1__tableItemShadow">
            <img src="<?=SITE_TEMPLATE_PATH?>/images/360.png" class="section1__tableItem360 fade-in">
        </div>
    </div>
    <svg xmlns="http://www.w3.org/2000/svg" preserveAspectRatio="none" viewBox="0 0 1000 100" class="section1__bottom">
        <path fill="white" stroke="white" stroke-width="1" d="M 0,0 L 0,100 L 1000,100 L 1000,0 Q 500,195 0,0"/>
    </svg>
    <div class="section__bg section1__bg">
        <div class="section__bgImage section1__bgImage parallax">
            <div class="section1__bgImageContainer"></div>
        </div>
    </div>
</section>


<section class="section section2">
    <div class="container">
        <div class="section__headGroup fade-in">
            <h2 class="section__header fade-in">Как это работает?</h2>
            <p class="section__subHeader fade-in">Все что Вам нужно это компьютер и фотоаппарат</p>
        </div>
        <ul class="stepList">
            <li class="stepList__item">
                <div class="stepList__itemNumber">1</div>
                <div class="stepList__itemName">Подключаем камеру к<br> компьютеру</div>
                <div class="stepList__itemDescription">Стол подключается и камера подключаются к компьютеру, на столешницу ставится объект. При помощи программы задается количество кадров (количество кадров, за полный круг оборота)</div>
            </li>
            <li class="stepList__item">
                <div class="stepList__itemNumber">2</div>
                <div class="stepList__itemName">Настраиваем программу Photo<br> Mix</div>
                <div class="stepList__itemDescription">Программа в автоматическом режиме фотографирует объект при круговом движении столешницы, а получаемое количество фотографий склеивает в один ролик.</div>
            </li>
            <li class="stepList__item">
                <div class="stepList__itemNumber">3</div>
                <div class="stepList__itemName">Ставим на стол предмет и<br> нажимаем “Пуск”</div>
                <div class="stepList__itemDescription">Стол подключается и камера подключаются к компьютеру, на столешницу ставится объект. При помощи программы задается количество кадров (количество кадров, за полный круг оборота)</div>
            </li>
        </ul>
    </div>
</section>


<section class="section section3">
    <div class="container">
        <div class="section__headGroup fade-in">
            <h2 class="section__header section3__header">Тех. характеристики<br>3D Photolab</h2>
            <p class="section__subHeader section3__subHeader">Технологии, проверенные опытом</p>
        </div>
        <ul class="pList fade-in">
            <li class="pList__item pList__item--diameter">Диаметр столешницы - 60 см.</li>
            <li class="pList__item pList__item--color">Цвет поверхности - белый</li>
            <li class="pList__item pList__item--temp">Допустимая температура<br>окружающей среды от +5 до + 40˚С</li>
            <li class="pList__item pList__item--weight">Вес предмета до 45 кг!</li>
            <li class="pList__item pList__item--material">Материал столешницы - дсп,<br /> покрытый меламином</li>
        </ul>
    </div>
    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1000 150" class="section3__corner">
        <path fill="white" stroke="white" stroke-width="1" d="M 0,0 L 1000,150 1000,0 0,0"/>
    </svg>
    <div class="section__bg section3__bg">
        <div class="section__bgImage section3__bgImage parallax"></div>
    </div>
    <div class="section3__camera">
        <img src="<?=SITE_TEMPLATE_PATH?>/images/camera.png" class="section3__cameraImage">
        <div class="section3__cameraBg"></div>
    </div>
</section>


<section class="section section4">
    <div class="container">
        <div class="section__headGroup fade-in">
            <h2 class="section__header section4__header">Увеличьте свой доход</h2>
            <p class="section__subHeader section4__subHeader">Более 200% в год рост применения технологии</p>
        </div>
        <div class="win">
            <ul class="winList">
                <li class="winList__item">
                    <div class="winList__itemNumber">40%</div>
                    <div class="winList__itemDescription">
                        Увеличение конверсии на покупку по сравнению с обычными фото товаров
                    </div>
                </li>
                <li class="winList__item">
                    <div class="winList__itemNumber">2</div>
                    <div class="winList__itemDescription">
                        месяца — срок окупаемости стола
                    </div>
                </li>
                <li class="winList__item">
                    <div class="winList__itemNumber">10$</div>
                    <div class="winList__itemDescription">
                        средняя стоимость одной 3D фотографии на рынке<br>
                        России
                    </div>
                </li>
            </ul>
        </div>
        <div class="pay">
            <div class="payBack">
                <h3 class="payBack__header fade-in">Заработай и прокачай свои навыки</h3>
                <div class="payBack__calculate fade-in">
                    <h4 class="payBack__calculate_header">Рассчитайте срок окупаемости стола</h4>
                    <form class="payBack__calculate_form">
                        <fieldset class="payBack__calculate_form_fieldset payBack__calculate_form_fieldset--1">
                            <label class="payBack__calculate_form_label_price  payBack__calculate_form_label">Стоимость фото, руб</label>
                            <input class="payBack__calculate_form_input_price payBack__calculate_form_input" type="text" value="500" id="calcPrice">
                        </fieldset>
                        <fieldset class="payBack__calculate_form_fieldset payBack__calculate_form_fieldset--2">
                            <label class="payBack__calculate_form_label_quantity payBack__calculate_form_label">Количество фото / мес.</label>
                            <input class="payBack__calculate_form_input_quantity payBack__calculate_form_input" type="text" value="100" id="calcQuantity">
                        </fieldset>
                        <fieldset class="payBack__calculate_form_fieldset  payBack__calculate_form_fieldset--3">
                            <label class="payBack__calculate_form_label_time payBack__calculate_form_label">Через сколько вы окупите стол</label>
                            <input class="payBack__calculate_form_input_time payBack__calculate_form_input" type="text" value="2 месяца" id="calcTime" disabled>
                        </fieldset>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <div class="section4__goods itemFly">
        <div class="section4__goodsBase">
            <img src="<?=SITE_TEMPLATE_PATH?>/images/item_bag.jpeg" class="section4__goodsItem section4__goodsItem--bag">
            <img src="<?=SITE_TEMPLATE_PATH?>/images/item_phone.png" class="section4__goodsItem section4__goodsItem--phone">
            <img src="<?=SITE_TEMPLATE_PATH?>/images/item_shoe.png" class="section4__goodsItem section4__goodsItem--shoe">
            <img src="<?=SITE_TEMPLATE_PATH?>/images/item_watch.png" class="section4__goodsItem section4__goodsItem--watch">
        </div>
    </div>
</section>

<section class="section section5">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-lg-6">
                <img class="section1__logo fade-in" src="<?=SITE_TEMPLATE_PATH?>/images/logo.png">
            </div>
            <div class="col-xs-12 col-lg-6">
                <h2 class="section__header section5__header fade-in">Примеры работ успешного фотографа Жуковой Юлии</h2>
                <blockquote class="section5__quote fade-in">“Это по-настоящему классное устройство. С помощью него я получила возможность выйти на крупных заказчиков”</blockquote>
            </div>
        </div>
        <?$APPLICATION->IncludeComponent("bitrix:news.list", "work_examples", Array(
            "ACTIVE_DATE_FORMAT" => "d.m.Y",	// Формат показа даты
            "ADD_SECTIONS_CHAIN" => "N",	// Включать раздел в цепочку навигации
            "AJAX_MODE" => "N",	// Включить режим AJAX
            "AJAX_OPTION_ADDITIONAL" => "",	// Дополнительный идентификатор
            "AJAX_OPTION_HISTORY" => "N",	// Включить эмуляцию навигации браузера
            "AJAX_OPTION_JUMP" => "N",	// Включить прокрутку к началу компонента
            "AJAX_OPTION_STYLE" => "Y",	// Включить подгрузку стилей
            "CACHE_FILTER" => "N",	// Кешировать при установленном фильтре
            "CACHE_GROUPS" => "Y",	// Учитывать права доступа
            "CACHE_TIME" => "36000000",	// Время кеширования (сек.)
            "CACHE_TYPE" => "A",	// Тип кеширования
            "CHECK_DATES" => "Y",	// Показывать только активные на данный момент элементы
            "DETAIL_URL" => "",	// URL страницы детального просмотра (по умолчанию - из настроек инфоблока)
            "DISPLAY_BOTTOM_PAGER" => "N",	// Выводить под списком
            "DISPLAY_DATE" => "N",	// Выводить дату элемента
            "DISPLAY_NAME" => "Y",	// Выводить название элемента
            "DISPLAY_PICTURE" => "Y",	// Выводить изображение для анонса
            "DISPLAY_PREVIEW_TEXT" => "N",	// Выводить текст анонса
            "DISPLAY_TOP_PAGER" => "N",	// Выводить над списком
            "FIELD_CODE" => array(	// Поля
                0 => "",
                1 => "",
            ),
            "FILTER_NAME" => "",	// Фильтр
            "HIDE_LINK_WHEN_NO_DETAIL" => "N",	// Скрывать ссылку, если нет детального описания
            "IBLOCK_ID" => "1",	// Код информационного блока
            "IBLOCK_TYPE" => "work_examples",	// Тип информационного блока (используется только для проверки)
            "INCLUDE_IBLOCK_INTO_CHAIN" => "N",	// Включать инфоблок в цепочку навигации
            "INCLUDE_SUBSECTIONS" => "N",	// Показывать элементы подразделов раздела
            "MESSAGE_404" => "",	// Сообщение для показа (по умолчанию из компонента)
            "NEWS_COUNT" => "4",	// Количество новостей на странице
            "PAGER_BASE_LINK_ENABLE" => "N",	// Включить обработку ссылок
            "PAGER_DESC_NUMBERING" => "N",	// Использовать обратную навигацию
            "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",	// Время кеширования страниц для обратной навигации
            "PAGER_SHOW_ALL" => "N",	// Показывать ссылку "Все"
            "PAGER_SHOW_ALWAYS" => "N",	// Выводить всегда
            "PAGER_TEMPLATE" => ".default",	// Шаблон постраничной навигации
            "PAGER_TITLE" => "Новости",	// Название категорий
            "PARENT_SECTION" => "",	// ID раздела
            "PARENT_SECTION_CODE" => "",	// Код раздела
            "PREVIEW_TRUNCATE_LEN" => "",	// Максимальная длина анонса для вывода (только для типа текст)
            "PROPERTY_CODE" => array(	// Свойства
                0 => "",
                1 => "",
            ),
            "SET_BROWSER_TITLE" => "N",	// Устанавливать заголовок окна браузера
            "SET_LAST_MODIFIED" => "N",	// Устанавливать в заголовках ответа время модификации страницы
            "SET_META_DESCRIPTION" => "N",	// Устанавливать описание страницы
            "SET_META_KEYWORDS" => "N",	// Устанавливать ключевые слова страницы
            "SET_STATUS_404" => "N",	// Устанавливать статус 404
            "SET_TITLE" => "N",	// Устанавливать заголовок страницы
            "SHOW_404" => "N",	// Показ специальной страницы
            "SORT_BY1" => "SORT",	// Поле для первой сортировки новостей
            "SORT_BY2" => "SORT",	// Поле для второй сортировки новостей
            "SORT_ORDER1" => "ASC",	// Направление для первой сортировки новостей
            "SORT_ORDER2" => "ASC",	// Направление для второй сортировки новостей
        ),
            false
        );?>
    </div>
    <div class="section__bg section5__bg">
        <div class="section__bgImage section5__bgImage parallax"></div>
    </div>
</section>

<section class="section section6">
    <div class="container">
        <div class="section__headGroup fade-in">
            <h2 class="section__header section6__header">Программа photo mix</h2>
            <p class="section__subHeader section6__subHeader">Управлять процессом съёмки проще простого</p>
        </div>
        <div class="section6__about">
            <div class="section6__screenshot fade-in">
                <img src="<?=SITE_TEMPLATE_PATH?>/images/screenshot.png" class="section6__screenshotImage">
            </div>
            <div class="section6__description fade-in">
                <p class="section6__descriptionText">Поворотный стол предназначен для создания 3D-фотографий - анимированных изображений на основе серии обычных фотографий, выполненных при разной ориентации объекта. На экране компьютера такое изображение можно вращать, разглядывая предмет со всех сторон.</p>
                <ul class="featureList">
                    <li class="featureListItem">Выбирайте количество кадров
                    <li class="featureListItem">Угол разворота
                    <li class="featureListItem">Скорость съёмки
                    <li class="featureListItem">Форматы фотографий
                    <li class="featureListItem">Широкие возможности
                </ul>
                <div class="section6__soBad">
                    Microsoft Windows<br>
                    32,64 Bit
                </div>
            </div>
        </div>
    </div>
</section>

<section class="section section7">
    <div class="container">
        <div class="section__headGroup fade-in">
            <h2 class="section__header">Время заказывать!</h2>
            <p class="section__subHeader section7__subHeader">
                Преврати свой поворотный стол в переносную<br>
                фото-лабораторию
            </p>
        </div>
        <div class="section7__options">
            <div class="section7__optionsOrder">
                <div class="order">
                    <div class="row">
                        <div class="col-xs-12 col-sm-6">
                            <div class="order__stars"></div>
                            <div class="order__price">
                                <div class="order__priceLabel">Цена:</div>
                                <div class="order__priceOld">56 000 <span class="rouble">a</span></div>
                                <div class="order__priceNew"><span id="price">56000</span> <span class="rouble">a</span></div>
                            </div>
                            <button class="order__button">Сделать заказ</button>
                        </div>
                        <div class="col-xs-12 col-sm-6 order__notes">
                            <h6 class="order__notesTitle">Комплектация стола:</h6>
                            <ul class="order__notesList">
                                <li class="order__notesListItem">USB-шнур</li>
                                <li class="order__notesListItem">Шнур питания</li>
                                <li class="order__notesListItem">Паспорт устройства</li>
                                <li class="order__notesListItem">Инструкция по эксплуатации</li>
                                <li class="order__notesListItem">Программа 3D-Photo Mix</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <div class="options__list section7__optionsList fade-in">
                <img src="<?=SITE_TEMPLATE_PATH?>/images/logo_black.png" class="section7__optionsListLogo">
                <ul class="featureList">
                    <li class="featureListItem">Поддерживает Nikon и Canon </li>
                    <li class="featureListItem">Гарантия 1 год.</li>
                    <li class="featureListItem">Годовая тех. поддержка</li>
                    <li class="featureListItem">Время создание фотографии менее 2 минут</li>
                    <li class="featureListItem">Не требует профессиональных навыков</li>
                    <li class="featureListItem">Стол удобен для транспортировки</li>
                </ul>
            </div>

        </div>
    </div>
    </section>
        <section class="section section8">
            <div class="container">
<?$APPLICATION->IncludeComponent("bitrix:news.list", "catalog", Array(
    "ACTIVE_DATE_FORMAT" => "d.m.Y",	// Формат показа даты
    "ADD_SECTIONS_CHAIN" => "N",	// Включать раздел в цепочку навигации
    "AJAX_MODE" => "N",	// Включить режим AJAX
    "AJAX_OPTION_ADDITIONAL" => "",	// Дополнительный идентификатор
    "AJAX_OPTION_HISTORY" => "N",	// Включить эмуляцию навигации браузера
    "AJAX_OPTION_JUMP" => "N",	// Включить прокрутку к началу компонента
    "AJAX_OPTION_STYLE" => "Y",	// Включить подгрузку стилей
    "CACHE_FILTER" => "N",	// Кешировать при установленном фильтре
    "CACHE_GROUPS" => "Y",	// Учитывать права доступа
    "CACHE_TIME" => "36000000",	// Время кеширования (сек.)
    "CACHE_TYPE" => "A",	// Тип кеширования
    "CHECK_DATES" => "Y",	// Показывать только активные на данный момент элементы
    "DETAIL_URL" => "",	// URL страницы детального просмотра (по умолчанию - из настроек инфоблока)
    "DISPLAY_BOTTOM_PAGER" => "N",	// Выводить под списком
    "DISPLAY_DATE" => "N",	// Выводить дату элемента
    "DISPLAY_NAME" => "Y",	// Выводить название элемента
    "DISPLAY_PICTURE" => "Y",	// Выводить изображение для анонса
    "DISPLAY_PREVIEW_TEXT" => "Y",	// Выводить текст анонса
    "DISPLAY_TOP_PAGER" => "N",	// Выводить над списком
    "FIELD_CODE" => array(	// Поля
        0 => "",
        1 => "",
    ),
    "FILTER_NAME" => "",	// Фильтр
    "HIDE_LINK_WHEN_NO_DETAIL" => "N",	// Скрывать ссылку, если нет детального описания
    "IBLOCK_ID" => "2",	// Код информационного блока
    "IBLOCK_TYPE" => "catalog",	// Тип информационного блока (используется только для проверки)
    "INCLUDE_IBLOCK_INTO_CHAIN" => "N",	// Включать инфоблок в цепочку навигации
    "INCLUDE_SUBSECTIONS" => "N",	// Показывать элементы подразделов раздела
    "MESSAGE_404" => "",	// Сообщение для показа (по умолчанию из компонента)
    "NEWS_COUNT" => "3",	// Количество новостей на странице
    "PAGER_BASE_LINK_ENABLE" => "N",	// Включить обработку ссылок
    "PAGER_DESC_NUMBERING" => "N",	// Использовать обратную навигацию
    "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",	// Время кеширования страниц для обратной навигации
    "PAGER_SHOW_ALL" => "N",	// Показывать ссылку "Все"
    "PAGER_SHOW_ALWAYS" => "N",	// Выводить всегда
    "PAGER_TEMPLATE" => ".default",	// Шаблон постраничной навигации
    "PAGER_TITLE" => "Новости",	// Название категорий
    "PARENT_SECTION" => "",	// ID раздела
    "PARENT_SECTION_CODE" => "",	// Код раздела
    "PREVIEW_TRUNCATE_LEN" => "",	// Максимальная длина анонса для вывода (только для типа текст)
    "PROPERTY_CODE" => array(	// Свойства
        0 => "OWN_DESIGN",
        1 => "PRICE",
        2 => "QUANTITY",
        3 => "",
    ),
    "SET_BROWSER_TITLE" => "N",	// Устанавливать заголовок окна браузера
    "SET_LAST_MODIFIED" => "N",	// Устанавливать в заголовках ответа время модификации страницы
    "SET_META_DESCRIPTION" => "N",	// Устанавливать описание страницы
    "SET_META_KEYWORDS" => "N",	// Устанавливать ключевые слова страницы
    "SET_STATUS_404" => "N",	// Устанавливать статус 404
    "SET_TITLE" => "N",	// Устанавливать заголовок страницы
    "SHOW_404" => "N",	// Показ специальной страницы
    "SORT_BY1" => "SORT",	// Поле для первой сортировки новостей
    "SORT_BY2" => "SORT",	// Поле для второй сортировки новостей
    "SORT_ORDER1" => "ASC",	// Направление для первой сортировки новостей
    "SORT_ORDER2" => "ASC",	// Направление для второй сортировки новостей
),
    false
);?>
            </div>
</section>


<?require($_SERVER['DOCUMENT_ROOT'].'/bitrix/footer.php');