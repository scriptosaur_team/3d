import Parallax from 'parallax-scroll'


const p = new Parallax(
    ".parallax",
    {
        speed: .2
    }
);
p.animate();
